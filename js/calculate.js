function daysBetween() {
		if($("#date1").val() == "" || $("#date2").val() == ""){
			alert("Please fill in both dates.");
			$("#results").html("The Answer Is: ");
		}
		
		else{
		
			var ONE_DAY = 1000 * 60 * 60 * 24;

			var date1_ms = $("#date1").val();
			var date2_ms = $("#date2").val();
			
			date1_ms = new Date(date1_ms);
			date2_ms = new Date(date2_ms);

			var difference_ms = Math.abs(date2_ms - date1_ms);
			
			$("#results").html("The Answer Is: " +  Math.round(difference_ms/ONE_DAY) + " days");
		}
}